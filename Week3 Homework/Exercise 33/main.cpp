#pragma comment(lib, "GLFWDLL")
#pragma comment(lib, "OpenGL32")
#pragma comment(lib, "glew32")

#define GLFW_DLL

#include <GL\glew.h>
#include <GL\glfw.h>
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <glm\gtc\quaternion.hpp>
#include <glm\gtx\constants.hpp>
#include <cstdlib>
#include <iostream>
#include "geometry.h"
#include "shader.h"

bool running = true;
GLuint shaders[2];
GLuint program;
GLint MVPuniform;
glm::mat4 projection;
GLint colourUniform;
render_object obj;
render_object obj2;
render_object obj3;

void init()
{
	glClearColor(0.0f, 1.0f, 1.0f, 1.0f);
	projection = glm::perspective(glm::degrees(glm::quarter_pi<float>()), 800.0f/600.0f, 0.1f, 10000.0f);
	glMatrixMode(GL_PROJECTION);
	glLoadMatrixf(glm::value_ptr(projection));
	glEnableClientState(GL_DEPTH_TEST);
	glEnableClientState(GL_VERTEX_ARRAY);
	shaders[0] = loadShader("transform.vert", GL_VERTEX_SHADER);
	shaders[1] = loadShader("transform.frag", GL_FRAGMENT_SHADER);
	if(shaders[0] && shaders[1])
	{
		program = createProgram(shaders, 2);
		if(!program)
		{
			exit(EXIT_FAILURE);
		}
	}
	else
	{
		exit(EXIT_FAILURE);
	}

	MVPuniform = glGetUniformLocation(program, "modelViewProjection");	
	colourUniform = glGetUniformLocation(program, "colour");

	geometry* geom = createBox();
	obj.geometry = geom;
	obj.colour = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);

	geometry* geom2 = createTorus(1.5f, 20, 30);
	obj2.geometry = geom2;
	obj2.colour = glm::vec4(0.0f, 1.0f, 0.0f, 1.0f);
	obj2.transform.position = glm::vec3(-4.0f, 2.0f, 1.0f);

	geometry* geom3 = createDisks(100);
	obj3.geometry = geom3;
	obj3.colour = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
	obj3.transform.position = glm::vec3(4.0f, -2.0f, 1.0f);
}

void update(double deltaTime)
{
	running = !glfwGetKey(GLFW_KEY_ESC) && glfwGetWindowParam(GLFW_OPENED);
}

void render(const glm::mat4& view, const glm::mat4& projection, const render_object& object)
{
	glm::mat4 mvp = projection * view * object.transform.getTransformMatrix();
	glUniformMatrix4fv(MVPuniform, 1, GL_FALSE, glm::value_ptr(mvp));
	glUniform4fv(colourUniform, 1, glm::value_ptr(object.colour));

	glBindVertexArray(object.geometry->vao);
	if(object.geometry->indexBuffer)
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, object.geometry->indexBuffer);
		glDrawElements(GL_TRIANGLES, object.geometry->indices.size(), GL_UNSIGNED_INT, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}
	else
		glDrawArrays(GL_TRIANGLES, 0, object.geometry->vertices.size());
	glBindVertexArray(0);
}

void render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glm::mat4 view = glm::lookAt(glm::vec3(10.0f, 10.0f, 10.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	glUseProgram(program);
	
	render(view, projection, obj);
	render(view, projection, obj2);
	render(view, projection, obj3);

	glUseProgram(0);
	glfwSwapBuffers();
}

void cleanup()
{
	if(program) glDeleteProgram(program);
	if(shaders[0]) glDeleteShader(shaders[0]);
	if(shaders[1]) glDeleteShader(shaders[1]);
}

int main(void)
{
	if(!glfwInit())
	{
		exit(EXIT_FAILURE);
	}
	if(!glfwOpenWindow(800, 600, 0, 0, 0, 0, 0, 0, GLFW_WINDOW))
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	GLenum error = glewInit();
	if(error != GLEW_OK)
	{
		std::cout << "Error: " << glewGetErrorString(error) << std::endl;
		exit(EXIT_FAILURE);
	}

	init();

	double lastTime = glfwGetTime();
	double currentTime;
	while(running)
	{
		currentTime = glfwGetTime();
		update(currentTime - lastTime);
		render();
		lastTime = glfwGetTime();
	}

	cleanup();

	glfwTerminate();

	exit(EXIT_SUCCESS);
}