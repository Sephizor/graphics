#pragma comment(lib, "GLFWDLL")
#pragma comment(lib, "OpenGL32")
#pragma comment(lib, "glew32")

#define GLFW_DLL

#include <GL\glew.h>
#include <GL\glfw.h>
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <glm\gtc\quaternion.hpp>
#include <glm\gtx\constants.hpp>
#include <cstdlib>
#include <iostream>
#include "geometry.h"
#include "shader.h"

bool running = true;
GLuint shaders[2];
GLuint program;
GLint colourUniform;
render_object object;
render_object tetrahedron;
render_object sphere;

void init()
{
	glClearColor(0.0f, 1.0f, 1.0f, 1.0f);
	glm::mat4 projection = glm::perspective(glm::degrees(glm::quarter_pi<float>()), 800.0f/600.0f, 0.1f, 10000.0f);
	glMatrixMode(GL_PROJECTION);
	glLoadMatrixf(glm::value_ptr(projection));
	glEnableClientState(GL_DEPTH_TEST);
	glEnableClientState(GL_VERTEX_ARRAY);
	shaders[0] = loadShader("colour.vert", GL_VERTEX_SHADER);
	shaders[1] = loadShader("colour.frag", GL_FRAGMENT_SHADER);
	if(shaders[0] && shaders[1])
	{
		program = createProgram(shaders, 2);
		if(!program)
		{
			exit(EXIT_FAILURE);
		}
	}
	else
	{
		exit(EXIT_FAILURE);
	}
	colourUniform = glGetUniformLocation(program, "colour");
	geometry* geom = createBox();
	object.geometry = geom;
	object.colour = glm::vec4(0.0f, 1.0f, 0.0f, 1.0f);
	
	geometry* geom2 = createTetrahedron();
	tetrahedron.geometry = geom2;
	tetrahedron.colour = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);
	tetrahedron.transform.position = glm::vec3(4.0f, 2.0f, -1.0f);

	geometry* geom3 = createSphere(10, 20);
	sphere.geometry = geom3;
	sphere.colour = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);
	sphere.transform.position = glm::vec3(-8.0f, -2.0f, -1.0f);
}

void update(double deltaTime)
{
	running = !glfwGetKey(GLFW_KEY_ESC) && glfwGetWindowParam(GLFW_OPENED);
}

void render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glm::mat4 view = glm::lookAt(glm::vec3(10.0f, 10.0f, 10.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	glMatrixMode(GL_MODELVIEW);
	glColor3fv(glm::value_ptr(object.colour));
	glUseProgram(program);
	glUniform4fv(colourUniform, 1, glm::value_ptr(object.colour));
	object.render(view);
	glUniform4fv(colourUniform, 1, glm::value_ptr(tetrahedron.colour));
	tetrahedron.render(view);
	glUniform4fv(colourUniform, 1, glm::value_ptr(sphere.colour));
	sphere.render(view);
	glUseProgram(0);
	glfwSwapBuffers();
}

void cleanup()
{
	if(program) glDeleteProgram(program);
	if(shaders[0]) glDeleteShader(shaders[0]);
	if(shaders[1]) glDeleteShader(shaders[1]);
	if(object.geometry) delete object.geometry;
}

int main(void)
{
	if(!glfwInit())
	{
		exit(EXIT_FAILURE);
	}
	if(!glfwOpenWindow(800, 600, 0, 0, 0, 0, 0, 0, GLFW_WINDOW))
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	GLenum error = glewInit();
	if(error != GLEW_OK)
	{
		std::cout << "Error: " << glewGetErrorString(error) << std::endl;
		exit(EXIT_FAILURE);
	}

	init();

	double lastTime = glfwGetTime();
	double currentTime;
	while(running)
	{
		currentTime = glfwGetTime();
		update(currentTime - lastTime);
		render();
		lastTime = glfwGetTime();
	}

	cleanup();

	glfwTerminate();

	exit(EXIT_SUCCESS);
}