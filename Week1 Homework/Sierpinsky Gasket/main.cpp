#pragma comment(lib, "GLFW")
#pragma comment(lib, "OpenGL32")

#include <GL\glfw.h>
#include <cstdlib>
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <vector>
#include <time.h>
#include <iostream>

const int numPoints = 500000;
glm::vec2 points[numPoints];
bool running = true;
std::vector<glm::vec2> vertices;
glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f);
float orientation = 0.0f;

void init()
{
	srand(time(0));
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glm::vec2 v[3] =
	{
		glm::vec2(-1.0, -1.0),
		glm::vec2(0.0, 1.0),
		glm::vec2(1.0, -1.0)
	};

	points[0] = glm::vec2(0.25, 0.50);
	
	for(int k=1; k<numPoints; k++) {
		int num = rand() %3;
		points[k] = (points[k-1] + v[num])/2.0f;
	}
}

void update(double deltaTime)
{
	running = !glfwGetKey(GLFW_KEY_ESC) && glfwGetWindowParam(GLFW_OPENED);
	if(glfwGetKey(GLFW_KEY_LEFT)) {
		position -= glm::vec3(0.1f, 0.0f, 0.0f);
	}
	if(glfwGetKey(GLFW_KEY_RIGHT)) {
		position += glm::vec3(0.1f, 0.0f, 0.0f);
	}
	if(glfwGetKey(GLFW_KEY_UP)) {
		position += glm::vec3(0.0f, 0.1f, 0.0f);
	}
	if(glfwGetKey(GLFW_KEY_DOWN)) {
		position -= glm::vec3(0.0f, 0.1f, 0.0f);
	}
	orientation += (deltaTime * 2.0f);
}

void render()
{
	glm::mat4 model = glm::rotate(glm::mat4(1.0f), glm::degrees(orientation), glm::vec3(1.0f, 1.0f, 1.0f));
	glm::mat4 translate = glm::translate(model, position);
	
	glMatrixMode(GL_MODELVIEW);
	glLoadMatrixf(glm::value_ptr(model));
	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(0.0f, 0.0f, 0.0f);
	glBegin(GL_POINTS);
		for(int k=0; k<numPoints; k++) {
			glVertex2fv(glm::value_ptr(points[k]));
		}
	glEnd();
	glfwSwapBuffers();
	glLoadIdentity();
}

int main(void)
{
	if(!glfwInit())
	{
		exit(EXIT_FAILURE);
	}

	if(!glfwOpenWindow(800, 600, 0, 0, 0, 0, 0, 0, GLFW_WINDOW))
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	init();

	double lastTime = glfwGetTime();
	double currentTime;

	while(running)
	{
		currentTime = glfwGetTime();
		update(currentTime - lastTime);
		render();
		lastTime = currentTime;
	}
}