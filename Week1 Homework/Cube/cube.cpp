#pragma comment(lib, "GLFW")
#pragma comment(lib, "OpenGL32")

#include <GL\glfw.h>
#include <cstdlib>
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <glm\gtx\constants.hpp>

bool running = true;
float rotationAmount = 0.001f;
glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f);
glm::vec3 rotation = glm::vec3(0.0f, 0.0f, 0.0f);
glm::vec3 scale = glm::vec3(1.0f, 1.0f, 1.0f);

void init()
{
	glClearColor(0.0f, 1.0f, 1.0f, 1.0f);
	glm::mat4 projection = glm::perspective(glm::degrees(glm::quarter_pi<float>()), 800.0f/600.0f, 0.1f, 10000.0f);
	glMatrixMode(GL_PROJECTION);
	glLoadMatrixf(glm::value_ptr(projection));
	glMatrixMode(GL_MODELVIEW);
	glEnable(GL_DEPTH_TEST);
}

void update(double deltaTime)
{
	running = !glfwGetKey(GLFW_KEY_ESC) && glfwGetWindowParam(GLFW_OPENED);
	//Scale
	if(glfwGetKey('Q')) {
		scale.x -= 0.01f;
	}
	if(glfwGetKey('W')) {
		scale.x += 0.01f;
	}
	if(glfwGetKey('E')) {
		scale.z += 0.01f;
	}
	if(glfwGetKey('R')) {
		scale.z -= 0.01f;
	}
	if(glfwGetKey('T')) {
		scale.y += 0.01f;
	}
	if(glfwGetKey('Y')) {
		scale.y -= 0.01f;
	}
	//Rotate
	if(glfwGetKey('A')) {
		rotation.x -= 0.01f;
	}
	if(glfwGetKey('S')) {
		rotation.x += 0.01f;
	}
	if(glfwGetKey('D')) {
		rotation.z += 0.01f;
	}
	if(glfwGetKey('F')) {
		rotation.z -= 0.01f;
	}
	if(glfwGetKey('G')) {
		rotation.y += 0.01f;
	}
	if(glfwGetKey('H')) {
		rotation.y -= 0.01f;
	}
	//Transform
	if(glfwGetKey(GLFW_KEY_LEFT)) {
		position.x -= 0.01f;
	}
	if(glfwGetKey(GLFW_KEY_RIGHT)) {
		position.x += 0.01f;
	}
	if(glfwGetKey(GLFW_KEY_UP)) {
		position.z += 0.01f;
	}
	if(glfwGetKey(GLFW_KEY_DOWN)) {
		position.z -= 0.01f;
	}
	if(glfwGetKey('Z')) {
		position.y += 0.01f;
	}
	if(glfwGetKey('X')) {
		position.y -= 0.01f;
	}
}

void render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glm::mat4 view = glm::lookAt(glm::vec3(10.0f, 10.0f, 10.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	glm::mat4 model = glm::scale(glm::mat4(1.0), scale);
	model = glm::rotate(model, glm::degrees(rotation.x), glm::vec3(1.0f, 0.0f, 0.0f));
	model = glm::rotate(model, glm::degrees(rotation.y), glm::vec3(0.0f, 0.1f, 0.0f));
	model = glm::rotate(model, glm::degrees(rotation.z), glm::vec3(0.0f, 0.0f, 0.1f));
	model = glm::translate(model, position);
	
	
	glMatrixMode(GL_MODELVIEW);
	glLoadMatrixf(glm::value_ptr(view * model));
	glBegin(GL_QUADS);
		//Face 1
		glColor3f(1.0f, 0.0f, 0.0f);
		glVertex3f(-1.0f, 1.0f, 1.0f);
		glVertex3f(1.0f, 1.0f, 1.0f);
		glVertex3f(1.0f, -1.0f, 1.0f);
		glVertex3f(-1.0f, -1.0f, 1.0f);
		//Face 2
		glColor3f(0.0f, 1.0f, 0.0f);
		glVertex3f(1.0f, 1.0f, 1.0f);
		glVertex3f(1.0f, 1.0f, -1.0f);
		glVertex3f(1.0f, -1.0f, -1.0f);
		glVertex3f(1.0f, -1.0f, 1.0f);
		//Face 3
		glColor3f(0.0f, 0.0f, 1.0f);
		glVertex3f(1.0f, 1.0f, -1.0f);
		glVertex3f(-1.0f, 1.0f, -1.0f);
		glVertex3f(-1.0f, -1.0f, -1.0f);
		glVertex3f(1.0f, -1.0f, -1.0f);
		//Face 4
		glColor3f(1.0f, 1.0f, 0.0f);
		glVertex3f(-1.0f, 1.0f, -1.0f);
		glVertex3f(-1.0f, 1.0f, 1.0f);
		glVertex3f(-1.0f, -1.0f, 1.0f);
		glVertex3f(-1.0f, -1.0f, -1.0f);
		//Face 5
		glColor3f(1.0f, 0.0f, 1.0f);
		glVertex3f(-1.0f, 1.0f, -1.0f);
		glVertex3f(1.0f, 1.0f, -1.0f);
		glVertex3f(1.0f, 1.0f, 1.0f);
		glVertex3f(-1.0f, 1.0f, 1.0f);
		//Face 6
		glColor3f(0.0f, 0.5f, 0.5f);
		glVertex3f(-1.0f, -1.0f, 1.0f);
		glVertex3f(1.0f, -1.0f, 1.0f);
		glVertex3f(1.0f, -1.0f, -1.0f);
		glVertex3f(-1.0f, -1.0f, -1.0f);
	glEnd();
	glLoadIdentity();
	glfwSwapBuffers();
}

int main(void)
{
	if(!glfwInit())
	{
		exit(EXIT_FAILURE);
	}

	if(!glfwOpenWindow(800, 600, 0, 0, 0, 0, 0, 0, GLFW_WINDOW))
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	init();

	double lastTime = glfwGetTime();
	double currentTime;

	while(running)
	{
		currentTime = glfwGetTime();
		update(currentTime - lastTime);
		render();
		lastTime = currentTime;
	}
}