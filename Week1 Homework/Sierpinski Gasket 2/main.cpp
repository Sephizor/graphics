#pragma comment(lib, "GLFW")
#pragma comment(lib, "OpenGL32")

#include <GL\glfw.h>
#include <cstdlib>
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <vector>
#include <time.h>
#include <iostream>

bool running = true;
std::vector<glm::vec2> vertices;

void triangle(const glm::vec2& a, const glm::vec2& b, const glm::vec2& c) {
	vertices.push_back(a);
	vertices.push_back(b);
	vertices.push_back(c);
}

void divide_triangle(const glm::vec2& a, const glm::vec2& b, const glm::vec2& c, int subdivisions) {

	if(subdivisions > 0) {
		float midpoint1x = (a.x + b.x) / 2;
		float midpoint1y = (a.y + b.y) / 2;

		float midpoint2x = (b.x + c.x) / 2;
		float midpoint2y = (b.y + c.y) / 2;
	
		float midpoint3x = (c.x + a.x) / 2;
		float midpoint3y = (c.y + a.y) / 2;

		divide_triangle(a, glm::vec2(midpoint1x, midpoint1y), glm::vec2(midpoint3x, midpoint3y), subdivisions-1);
		divide_triangle(glm::vec2(midpoint1x, midpoint1y), b, glm::vec2(midpoint2x, midpoint2y), subdivisions-1);
		divide_triangle(glm::vec2(midpoint3x, midpoint3y), glm::vec2(midpoint2x, midpoint2y), c, subdivisions-1);
	}
	else {
		triangle(a,b,c);
	}

}

void init() {
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glEnable(GL_VERTEX_ARRAY);
	divide_triangle(glm::vec2(-1.0f, -1.0f), glm::vec2(0.0f, 1.0f), glm::vec2(1.0f, -1.0f), 10);
}


void update(double deltaTime) {

}

void render() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glVertexPointer(2, GL_FLOAT, 0, &vertices[0]);
	glColor3f(1.0f, 0.0f, 0.0f);
	glDrawArrays(GL_TRIANGLES, 0, vertices.size());
	glfwSwapBuffers();
}

int main(void)
{
	if(!glfwInit())
	{
		exit(EXIT_FAILURE);
	}

	if(!glfwOpenWindow(800, 600, 0, 0, 0, 0, 0, 0, GLFW_WINDOW))
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	init();

	double lastTime = glfwGetTime();
	double currentTime;

	while(running)
	{
		currentTime = glfwGetTime();
		update(currentTime - lastTime);
		render();
		lastTime = currentTime;
	}
}