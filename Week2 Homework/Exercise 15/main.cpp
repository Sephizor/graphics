#pragma comment(lib, "GLFW")
#pragma comment(lib, "OpenGL32")

#include <GL\glfw.h>
#include <cstdlib>
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <glm\gtx\constants.hpp>
#include "geometry.h"

bool running = true;
render_object cube;
render_object cube2;
geometry geom;

unsigned int indices[36] =
{
	//Side 1
	0, 1, 3,
	1, 2, 3,
	//Side 2
	1, 4, 2,
	4, 7, 2,
	//Side 3
	4, 5, 7,
	5, 6, 7,
	//Side 4
	5, 0, 6,
	0, 3, 6,
	//Side 5
	5, 4, 0,
	4, 1, 0,
	//Side 6
	3, 2, 6,
	2, 7, 6
};

void init()
{
	glClearColor(0.0f, 1.0f, 1.0f, 1.0f);
	glm::mat4 projection = glm::perspective(glm::degrees(glm::quarter_pi<float>()), 800.0f/600.0f, 0.1f, 10000.0f);
	glMatrixMode(GL_PROJECTION);
	glLoadMatrixf(glm::value_ptr(projection));
	glMatrixMode(GL_MODELVIEW);
	glEnable(GL_DEPTH_TEST);
	glEnableClientState(GL_VERTEX_ARRAY);
	geom.vertices.push_back(glm::vec3(-1.0f, 1.0f, 1.0f));
	geom.vertices.push_back(glm::vec3(1.0f, 1.0f, 1.0f));
	geom.vertices.push_back(glm::vec3(1.0f, -1.0f, 1.0f));
	geom.vertices.push_back(glm::vec3(-1.0f, -1.0f, 1.0f));
	geom.vertices.push_back(glm::vec3(1.0f, 1.0f, -1.0f));
	geom.vertices.push_back(glm::vec3(-1.0f, 1.0f, -1.0f));
	geom.vertices.push_back(glm::vec3(-1.0f, -1.0f, -1.0f));
	geom.vertices.push_back(glm::vec3(1.0f, -1.0f, -1.0f));
	for(int i=0; i<36; i++) {
		geom.indices.push_back(indices[i]);
	}
	cube.geometry = &geom;
	cube2.geometry = &geom;
	cube.colour = glm::vec3(1.0f, 0.0f, 0.0f);
	cube2.colour = glm::vec3(0.0f, 0.0f, 1.0f);
	cube.transform.position = glm::vec3(0.0f, 0.0f, 2.0f);
	cube2.transform.position = glm::vec3(1.0f, 2.0f, 0.0f);
}

void update(double deltaTime)
{
	running = !glfwGetKey(GLFW_KEY_ESC) && glfwGetWindowParam(GLFW_OPENED);
	if(glfwGetKey(GLFW_KEY_UP)) {
		cube.transform.move(glm::vec3(0.0f, 0.01f, 0.0f));
	}
	if(glfwGetKey(GLFW_KEY_DOWN)) {
		cube.transform.move(glm::vec3(0.0f, -0.01f, 0.0f));
	}
	if(glfwGetKey(GLFW_KEY_LEFT)) {
		cube.transform.move(glm::vec3(-0.01f, 0.0f, 0.0f));
	}
	if(glfwGetKey(GLFW_KEY_RIGHT)) {
		cube.transform.move(glm::vec3(0.01f, 0.0f, 0.0f));
	}
	if(glfwGetKey('W')) {
		cube.transform.move(glm::vec3(0.0f, 0.0f, 0.01f));
	}
	if(glfwGetKey('S')) {
		cube.transform.move(glm::vec3(0.0f, 0.0f, -0.01f));
	}
}

void render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glm::mat4 view = glm::lookAt(glm::vec3(10.0f, 10.0f, 10.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	glMatrixMode(GL_MODELVIEW);
	cube.render(view);
	glfwSwapBuffers();
}

int main(void)
{
	if(!glfwInit())
	{
		exit(EXIT_FAILURE);
	}

	if(!glfwOpenWindow(800, 600, 0, 0, 0, 0, 0, 0, GLFW_WINDOW))
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	init();

	double lastTime = glfwGetTime();
	double currentTime;

	while(running)
	{
		currentTime = glfwGetTime();
		update(currentTime - lastTime);
		render();
		lastTime = currentTime;
	}
}