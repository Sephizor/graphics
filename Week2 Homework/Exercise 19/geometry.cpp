#include "geometry.h"


glm::vec3 tetrahedron_vertices[4] =
{
	glm::vec3(0.0f, 1.0f, 0.0f),
	glm::vec3(-1.0f, -1.0f, 1.0f),
	glm::vec3(1.0f,-1.0f, 1.0f),
	glm::vec3(0.0f,-1.0f, -1.0f)
};

glm::vec3 pyramid_vertices[5] =
{
	glm::vec3(0.0f, 1.0f, 0.0f),
	glm::vec3(-1.0f, -1.0f, 1.0f),
	glm::vec3(1.0f, -1.0f, 1.0f),
	glm::vec3(1.0f, -1.0f, -1.0f),
	glm::vec3(-1.0f, -1.0f, -1.0f),
};

glm::vec3 box_vertices[8] =
{
	glm::vec3(-1.0f, 1.0f,1.0f),
	glm::vec3(1.0f, 1.0f,1.0f),
	glm::vec3(1.0f,-1.0f,1.0f),
	glm::vec3(-1.0f,-1.0f,1.0f),
	glm::vec3(1.0f, 1.0f,-1.0f),
	glm::vec3(-1.0f,1.0f,-1.0f),
	glm::vec3(-1.0f,-1.0f,-1.0f),
	glm::vec3(1.0f,-1.0f,-1.0f)
};

unsigned int tetrahedron_indices[12] = 
{
	//Side 1
	0, 2, 1,
	//Side 2
	0, 3, 2,
	//Side 3
	0, 1, 3,
	//Bottom
	1, 2, 3
};

unsigned int pyramid_indices[18] =
{
	0, 2, 1,
	0, 3, 2,
	0, 4, 3,
	0, 1, 4,
	1, 3, 4,
	1, 2, 3
};

unsigned int box_indices[36] = 
{
	//Side 1
	0, 1, 3,
	1, 2, 3,
	//Side 2
	1, 4, 2,
	4, 7, 2,
	//Side 3
	4, 5, 7,
	5, 6, 7,
	//Side 4
	5, 0, 6,
	0, 3, 6,
	//Side 5
	5, 4, 0,
	4, 1, 0,
	//Side 6	
	3, 2, 6,
	2, 7, 6
};

geometry* createBox()
{
	geometry* geom = new geometry();
	for(int i = 0;i < 8;++i)
		geom->vertices.push_back(box_vertices[i]);
	for(int i = 0;i < 36;++i)
		geom->indices.push_back(box_indices[i]);
	return geom;
}

geometry* createTetrahedron()
{
	geometry* geom = new geometry();
	for(int i = 0;i < 4;++i)
		geom->vertices.push_back(tetrahedron_vertices[i]);
	for(int i = 0;i < 12;++i)
		geom->indices.push_back(tetrahedron_indices[i]);
	return geom;
}

geometry* createPyramid()
{
	geometry* geom = new geometry();
	for(int i = 0;i < 5;++i)
		geom->vertices.push_back(pyramid_vertices[i]);
	for(int i = 0;i < 18;++i)
		geom->indices.push_back(pyramid_indices[i]);
	return geom;
}
