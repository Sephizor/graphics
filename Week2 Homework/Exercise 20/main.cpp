#pragma comment(lib, "GLFW")
#pragma comment(lib, "OpenGL32")

#include "geometry.h"
#include <glm\glm.hpp>
#include <glm\gtx\constants.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <glm\gtc\quaternion.hpp>

#include <GL\glfw.h>
#include <cstdlib>

bool running = true;
glm::quat quaternion;

render_object disk;
render_object disk2;

void initialise()
{
	glClearColor(0.0f, 1.0f, 1.0f, 1.0f);
	glm::mat4 projection = glm::perspective(glm::degrees(glm::quarter_pi<float>()), 800.0f/600.0f, 0.1f, 10000.0f);

	glMatrixMode(GL_PROJECTION);
	glLoadMatrixf(glm::value_ptr(projection));
	glMatrixMode(GL_MODELVIEW);
	glEnable(GL_DEPTH_TEST);
	glEnableClientState(GL_VERTEX_ARRAY);
	
	geometry* geom = createDisks(100);
	disk.geometry = geom;
	disk.colour = glm::vec3(1.0f, 0.0f, 0.0f);

	geometry* geom2 = createDisks(100);
	disk2.geometry = geom2;
	disk2.colour = glm::vec3(0.0f, 0.0f, 1.0f);
	disk2.transform.position = glm::vec3(1.0f, -1.0f, -1.0f);
}

void update(double deltaTime)
{
	running = !glfwGetKey(GLFW_KEY_ESC) && glfwGetWindowParam(GLFW_OPENED);
	
	if (glfwGetKey(GLFW_KEY_UP))
	{
		disk.transform.move(glm::vec3(0.0f, 0.1f, 0.0f));
	}
	if (glfwGetKey(GLFW_KEY_DOWN))
	{
		disk.transform.move(glm::vec3(0.0f, -0.1f, 0.0f));
	}
	if (glfwGetKey(GLFW_KEY_LEFT))
	{
		disk.transform.move(glm::vec3(-0.1f, 0.0f, 0.0f));
	}
	if (glfwGetKey(GLFW_KEY_RIGHT))
	{
		disk.transform.move(glm::vec3(0.1f, 0.0f, 0.0f));
	}
	if (glfwGetKey('W'))
	{
		disk.transform.move(glm::vec3(0.0f, 0.0f, -0.1f));
	}
	if (glfwGetKey('S'))
	{
		disk.transform.move(glm::vec3(0.0f, 0.0f, 0.1f));
	}

	if (glfwGetKey('Z'))
	{
		disk2.transform.move(glm::vec3(0.1f, 0.0f, 0.0f));
	}
	if (glfwGetKey('X'))
	{
		disk2.transform.move(glm::vec3(-0.1f, 0.0f, 0.0f));
	}
	if (glfwGetKey('C'))
	{
		disk2.transform.move(glm::vec3(0.0f, 0.1f, 0.0f));
	}
	if (glfwGetKey('V'))
	{
		disk2.transform.move(glm::vec3(0.0f, -0.1f, 0.0f));
	}
	if (glfwGetKey('B'))
	{
		disk2.transform.move(glm::vec3(0.0f, 0.0f, 0.1f));
	}
	if (glfwGetKey('N'))
	{
		disk2.transform.move(glm::vec3(0.0f, 0.0f, -0.1f));
	}
}

void render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glm::mat4 view = glm::lookAt(glm::vec3(10.0f, 10.0f, 10.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	glMatrixMode(GL_MODELVIEW);
	disk.render(view);
	disk2.render(view);
	glfwSwapBuffers();
}

void main()
{
	if(!glfwInit())
		exit(EXIT_FAILURE);

	if(!glfwOpenWindow(800, 600, 0, 0, 0, 0, 0, 0, GLFW_WINDOW))
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	initialise();

	double prevTimeStamp = glfwGetTime();
	double currentTimeStamp;
	while(running)
	{
		currentTimeStamp = glfwGetTime();
		update(currentTimeStamp - prevTimeStamp);
		render();
		prevTimeStamp = currentTimeStamp;
	}

	glfwTerminate();

	exit(EXIT_SUCCESS);
}