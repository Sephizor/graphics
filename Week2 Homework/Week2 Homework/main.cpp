#pragma comment(lib, "GLFW")
#pragma comment(lib, "OpenGL32")

#include <GL\glfw.h>
#include <cstdlib>
#include <vector>
#include <iostream>
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <glm\gtx\constants.hpp>
#include <glm\gtx\quaternion.hpp>

bool running = true;
glm::quat quarternation;

float vertices[24] =
{
	-1.0f, 1.0f, 1.0f, //0
	1.0f, 1.0f, 1.0f, //1
	1.0f, -1.0f, 1.0f, //2
	-1.0f, -1.0f, 1.0f, //3
	1.0f, 1.0f, -1.0f, //4
	-1.0f, 1.0f, -1.0f, //5
	-1.0f, -1.0f, -1.0f, //6
	1.0f, -1.0f, -1.0f //7
};

unsigned int indices[36] =
{
	//Side 1
	0, 1, 3,
	1, 2, 3,
	//Side 2
	1, 4, 2,
	4, 7, 2,
	//Side 3
	4, 5, 7,
	5, 6, 7,
	//Side 4
	5, 0, 6,
	0, 3, 6,
	//Side 5
	5, 4, 0,
	4, 1, 0,
	//Side 6
	3, 2, 6,
	2, 7, 6
};

void init()
{
	glClearColor(0.0f, 1.0f, 1.0f, 1.0f);
	glm::mat4 projection = glm::perspective(glm::degrees(glm::quarter_pi<float>()), 800.0f/600.0f, 0.1f, 1000.0f);
	glMatrixMode(GL_PROJECTION);
	glLoadMatrixf(glm::value_ptr(projection));
	glMatrixMode(GL_MODELVIEW);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_VERTEX_ARRAY);
}

void update(double deltaTime)
{
	running = !glfwGetKey(GLFW_KEY_ESC) && glfwGetWindowParam(GLFW_OPENED);

	if(glfwGetKey(GLFW_KEY_UP)) {
		quarternation = glm::rotate(quarternation, -glm::degrees(glm::pi<float>() / 1000.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	}
	if(glfwGetKey(GLFW_KEY_DOWN)) {
		quarternation = glm::rotate(quarternation, glm::degrees(glm::pi<float>() / 1000.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	}
	if(glfwGetKey(GLFW_KEY_LEFT)) {
		quarternation = glm::rotate(quarternation, -glm::degrees(glm::pi<float>() / 1000.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	}
	if(glfwGetKey(GLFW_KEY_RIGHT)) {
		quarternation = glm::rotate(quarternation, glm::degrees(glm::pi<float>() / 1000.0f), glm::vec3(1.0f, 0.0f, 0.0f));
	}
	if(glfwGetKey('W')) {
		quarternation = glm::rotate(quarternation, glm::degrees(glm::pi<float>() / 1000.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	}
	if(glfwGetKey('S')) {
		quarternation = glm::rotate(quarternation, -glm::degrees(glm::pi<float>() / 1000.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	}

}

void render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glm::mat4 view = glm::lookAt(glm::vec3(10.0f, 10.0f, 10.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	glm::mat4 model = glm::mat4_cast(quarternation);
	glMatrixMode(GL_MODELVIEW);
	glLoadMatrixf(glm::value_ptr(view * model));
	glVertexPointer(3, GL_FLOAT, 0, vertices);
	glColor3f(1.0f, 0.0f, 0.0f);
	glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, indices);
	glfwSwapBuffers();
}

int main(void)
{
	if(!glfwInit())
	{
		exit(EXIT_FAILURE);
	}

	if(!glfwOpenWindow(800, 600, 0, 0, 0, 0, 0, 0, GLFW_WINDOW))
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	init();

	double lastTime = glfwGetTime();
	double lastTimeDrawn = glfwGetTime();
	double onesixtyth = (double) 1/60;
	std::cout << onesixtyth;
	int frames = 0, fps = 0;
	double currentTime;

	while(running)
	{
		currentTime = glfwGetTime();
		update(currentTime - lastTime);
		render();
		lastTime = currentTime;
	}
}