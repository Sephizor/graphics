#pragma once

#include <string>
#include <GL\glew.h>
#include <hash_map>
#include "geometry.h"

GLuint loadShader(const std::string& filename, GLenum type);
GLuint createProgram(GLuint* shaders, int count);

class shader
{
public:
	GLuint program;
	GLuint shaders[2];
	GLint lightUniform;
	GLint materialUniform;
	std::hash_map<std::string, GLint> uniforms;

	shader(const std::string& name);
	~shader();

	void setMaterial(GLuint materialBuffer);
	void setLight(GLuint lightBuffer);
};

struct lighting
{
	glm::vec4 ambient;
	glm::vec4 diffuse;
	glm::vec4 specular;
	glm::vec3 lightDir;
};