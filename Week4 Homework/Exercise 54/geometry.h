#pragma once

#include <vector>
#include <glm\glm.hpp>
#include <glm\gtx\constants.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <glm\gtc\quaternion.hpp>
#include <GL\glew.h>
#include <GL\glfw.h>

struct geometry
{
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> normals;
	std::vector<unsigned int> indices;

	GLuint vertexBuffer;
	GLuint indexBuffer;
	GLuint normalBuffer;
	GLuint vao;

	geometry()
		: vertexBuffer(0), normalBuffer(0), vao(0), indexBuffer(0)
	{
	}

	~geometry()
	{
		if(vertexBuffer)
		{
			glDeleteBuffers(1, &vertexBuffer);
			vertexBuffer = 0;
		}
		if(indexBuffer)
		{
			glDeleteBuffers(1, &indexBuffer);
			indexBuffer = 0;
		}
		if(normalBuffer)
		{
			glDeleteBuffers(1, &normalBuffer);
			normalBuffer = 0;
		}
		if(vao)
		{
			glDeleteVertexArrays(1, &vao);
			vao = 0;
		}
	}
};

struct transform
{
	glm::vec3 position;
	glm::quat rotation;
	glm::vec3 scale;

	transform()
	{
		scale = glm::vec3(1.0f,1.0f,1.0f);
	}

	void move(const glm::vec3& movement)
	{
		position += movement;
	}

	void rotate(float angle, const glm::vec3& axis)
	{
		rotation = glm::rotate(rotation, glm::degrees(angle),axis);
	}

	glm::mat4 getTransformMatrix() const
	{
		glm::mat4 matrix = glm::translate(glm::mat4(1.0f), position);
		matrix = glm::scale(matrix,scale);
		glm::mat4 rotMatrix = glm::mat4_cast(rotation);
		matrix *= rotMatrix;
		return matrix;
	}
};

struct material
{
	glm::vec4 emissive;
	glm::vec4 ambient;
	glm::vec4 diffuse;
	glm::vec4 specular;
	float shininess;
	float padding[3];
};

struct render_object
{
	geometry* geometry;
	transform transform;
	material* material;
	GLuint materialBuffer;
};

material* createMaterial(const glm::vec4& emissive, const glm::vec4& ambient, const glm::vec4& diffuse, const glm::vec4& specular, float shininess);
GLuint createMaterialBuffer(const material* mat);

geometry* createBox();
geometry* createTetrahedron();
geometry* createPyramid();
geometry* createDisks(int slices);
geometry* createCylinder(int stacks, int slices);
geometry* createSphere(int stacks, int slices);
geometry* createTorus(float radius, int stacks, int slices);
geometry* createPlane(int width, int depth);