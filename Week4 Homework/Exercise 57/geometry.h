#pragma once

#include <vector>
#include <GL\glew.h>
#include <GL\glfw.h>
#include <glm\glm.hpp>

struct geometry
{
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> normals;
	std::vector<unsigned int> indices;

	GLuint vertexBuffer;
	GLuint indexBuffer;
	GLuint normalBuffer;
	GLuint vao;

	geometry()
		: vertexBuffer(0), normalBuffer(0), vao(0), indexBuffer(0)
	{
	}

	~geometry()
	{
		if(vertexBuffer) glDeleteBuffers(1, &vertexBuffer);
		if(normalBuffer) glDeleteBuffers(1, &normalBuffer);
		if(indexBuffer) glDeleteBuffers(1, &indexBuffer);
		if(vao) glDeleteVertexArrays(1, &vao);
	}
};

geometry* createBox();
geometry* createTetrahedron();
geometry* createPyramid();
geometry* createDisk(int slices);
geometry* createCylinder(int stacks, int slices);
geometry* createSphere(int stacks, int slices);
geometry* createSphere(int divisions);
geometry* createTorus(float radius, int stacks, int slices);
geometry* createPlane(int width, int depth);
geometry* createSierpinski(int divisions);