#pragma comment(lib, "GLFWDLL")
#pragma comment(lib, "OpenGL32")
#pragma comment(lib, "glew32")
#pragma comment(lib, "DevIL")
#pragma comment(lib, "ILU")
#pragma comment(lib, "ILUT")

#define GLFW_DLL

#include <GL\glew.h>
#include <GL\glfw.h>
#include <IL\ilut.h>
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <glm\gtc\quaternion.hpp>
#include <glm\gtx\constants.hpp>
#include <cstdlib>
#include <iostream>
#include <time.h>
#include "render_object.h"
#include "effect.h"
#include "light.h"
#include "scene.h"
#include "camera.h"

effect eff;
render_object object;
target_camera* cam;

glm::vec3 lightAim(0.0f, 0.0f, 0.0f);
float power = 1.0f;

bool running = true;

void init()
{
	srand(time(NULL));

	glEnable(GL_DEPTH_TEST);
	glEnableClientState(GL_VERTEX_ARRAY);

	glClearColor(0.0f, 1.0f, 1.0f, 1.0f);
	cam = new target_camera();
	cam->setProjection(glm::pi<float>() / 4.0f, 800.0f/600.0f, 0.1f, 10000.0f);
	cam->setTarget(glm::vec3(0.0f, 0.0f, 0.0f));
	cam->setPosition(glm::vec3(10.0f, 10.0f, 10.0f));
	
	if(!eff.addShader("texture.vert", GL_VERTEX_SHADER))
		exit(EXIT_FAILURE);
	if(!eff.addShader("texture.frag", GL_FRAGMENT_SHADER))
		exit(EXIT_FAILURE);
	if(!eff.create())
		exit(EXIT_FAILURE);

	//scene = loadScene("scene.json");
	object.geometry = createBox();
	object.material = new material();
	object.material->data.ambient = glm::vec4(0.5f, 0.25f, 0.15f, 1.0f);
	object.material->data.diffuse = glm::vec4(0.5f, 0.25f, 0.15f, 1.0f);
	object.material->data.specular = glm::vec4(0.5f, 0.25f, 0.15f, 1.0f);
	object.material->data.emissive = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	object.material->texture = ilutGLLoadImage((wchar_t*)"brick.jpg");
	object.material->create();
}

void update(double deltaTime)
{
	cam->update(deltaTime);
	running = !glfwGetKey(GLFW_KEY_ESC) && glfwGetWindowParam(GLFW_OPENED);
	if(glfwGetKey('O'))
		power = std::max<float>(power - 0.1f, 0.0f);
	if(glfwGetKey('P'))
		power += 0.1f;
	if(glfwGetKey(GLFW_KEY_UP))
		object.transform.rotate(glm::pi<float>() / 100.0f, glm::vec3(1.0f, 0.0f, 0.0f));
	if(glfwGetKey(GLFW_KEY_DOWN))
		object.transform.rotate(-glm::pi<float>() / 100.0f, glm::vec3(1.0f, 0.0f, 0.0f));
	if(glfwGetKey(GLFW_KEY_LEFT))
		object.transform.rotate(glm::pi<float>() / 100.0f, glm::vec3(0.0f, 1.0f, 0.0f));
	if(glfwGetKey(GLFW_KEY_RIGHT))
		object.transform.rotate(-glm::pi<float>() / 100.0f, glm::vec3(0.0f, 1.0f, 0.0f));
}

void render(const effect* eff, const glm::mat4& view, const glm::mat4& projection, const render_object* object)
{
	glm::mat4 mvp = projection * view * object->transform.getTransformMatrix();
	glUniformMatrix4fv(eff->getUniformIndex("modelViewProjection"), 1, GL_FALSE, glm::value_ptr(mvp));

	object->material->bind(eff);

	glBindVertexArray(object->geometry->vao);
	if(object->geometry->indexBuffer)
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, object->geometry->indexBuffer);
		glDrawElements(GL_TRIANGLES, object->geometry->indices.size(), GL_UNSIGNED_INT, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}
	else
		glDrawArrays(GL_TRIANGLES, 0, object->geometry->vertices.size());
	glBindVertexArray(0);
}

void render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//glm::mat4 view = glm::lookAt(glm::vec3(10.0f, 10.0f, 10.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	eff.begin();
	render(&eff, cam->getView(), cam->getProjection(), &object);
	eff.end();

	//Create the light direction from lightAngle
	/*spot_light_data spot = scene->dynamic.data.spots[0];
	spot.lightDir = glm::vec4(lightAim, 1.0f) - spot.lightPos;
	spot.lightDir = glm::normalize(spot.lightDir);
	spot.power = power;
	//Set the light
	scene->dynamic.setSpot(0, spot);

	scene->dynamic.bind(&eff);
	scene->light.bind(&eff);

	glUniform3fv(eff.getUniformIndex("eyePos"), 1, glm::value_ptr(glm::vec3(10.0f, 10.0f, 10.0f)));

	std::hash_map<std::string, render_object*>::const_iterator iter = scene->objects.begin();
	for(; iter != scene->objects.end(); iter++)
		render(&eff, cam->getView(), cam->getProjection(), iter->second);*/
	//glUseProgram(0);
	glfwSwapBuffers();
}

void cleanup()
{
}

int main(void)
{
	if(!glfwInit())
		exit(EXIT_FAILURE);
	if(!glfwOpenWindow(800, 600, 0, 0, 0, 0, 0, 0, GLFW_WINDOW))
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	GLenum error = glewInit();
	if(error != GLEW_OK)
	{
		std::cout << "Error: " << glewGetErrorString(error) << std::endl;
		exit(EXIT_FAILURE);
	}

	ilInit();
	iluInit();
	ilutRenderer(ILUT_OPENGL);

	const GLubyte* renderer = glGetString(GL_RENDERER);
	const GLubyte* vendor = glGetString(GL_VENDOR);
	const GLubyte* version = glGetString(GL_VERSION);
	const GLubyte* glslVersion = glGetString(GL_SHADING_LANGUAGE_VERSION);
	GLint major, minor;
	glGetIntegerv(GL_MAJOR_VERSION, &major);
	glGetIntegerv(GL_MINOR_VERSION, &minor);
	printf("GL Vendor: %s\n", vendor);
	printf("GL Renderer: %s\n", renderer);
	printf("GL Version (string): %s\n", version);
	printf("GL Version (integer): %d.%d\n", major, minor);
	printf("GLSL Version: %s\n", glslVersion);

	init();

	double lastTime = glfwGetTime();
	double thisTime;
	while(running)
	{
		thisTime = glfwGetTime();
		update(thisTime - lastTime);
		render();
		lastTime = thisTime;
	}

	cleanup();

	glfwTerminate();

	exit(EXIT_SUCCESS);
}