#version 330

uniform mat4 modelViewProjection;
uniform mat4 modelInverseTranspose;
uniform mat4 model;

uniform vec4 ambientLight;
uniform vec4 diffuseLight;
uniform vec4 specularLight;

uniform vec4 ambientMaterial;
uniform vec4 diffuseMaterial;
uniform vec4 specularMaterial;
uniform float shininess;

uniform vec3 lightDir;
uniform vec3 eyePos;

in vec3 transformedPosition;
smooth in vec3 transformedNormal;

out vec4 colour;

void main()
{
	//Ambient Colour
	vec4 ambientColour = ambientMaterial * ambientLight;

	//Diffuse Colour
	float s = max(dot(lightDir, transformedNormal), 0.0);
	vec4 diffuseColour = s * (diffuseMaterial * diffuseLight);
	
	//Specular Colour
	vec3 toEye = normalize(eyePos - transformedPosition);
	vec3 r = reflect(-lightDir, transformedNormal);
	float t =  pow(max(dot(r, toEye), 0.0), shininess);
	vec4 specularColour = t * (specularMaterial * specularLight);

	colour = ambientColour + diffuseColour + specularColour;
	colour.a = 1.0;
}