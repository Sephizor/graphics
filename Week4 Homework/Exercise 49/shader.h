#pragma once

#include <string>
#include <GL\glew.h>
#include <hash_map>
#include "geometry.h"

GLuint loadShader(const std::string& filename, GLenum type);
GLuint createProgram(GLuint* shaders, int count);

class shader
{
public:
	GLuint program;
	GLuint shaders[2];
	std::hash_map<std::string, GLint> uniforms;

	shader(const std::string& name);
	~shader();

	void setMaterial(const material* material);
	void setLight(const lighting* light);
};