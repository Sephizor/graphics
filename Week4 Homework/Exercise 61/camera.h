#pragma once

#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtx\quaternion.hpp>
#include <glm\gtx\euler_angles.hpp>

class camera
{
protected:
	glm::vec3 _position;
	glm::vec3 _target;
	glm::vec3 _up;
	glm::mat4 _view;
	glm::mat4 _projection;
public:
	camera() { }
	virtual ~camera() { }
	virtual void update(float deltaTime) = 0;
	glm::vec3 getPosition() const { return _position; }
	void setPosition(const glm::vec3& value) { _position = value; }
	glm::vec3 getTarget() const { return _target; }
	void setTarget(const glm::vec3& value) { _target = value; }
	glm::vec3 getUp() const { return _up; }
	void setUp(const glm::vec3& value) { _up = value; }
	glm::mat4 getView() const { return _view; }
	glm::mat4 getProjection() const { return _projection; }
	void setProjection(float fov, float aspect, float nearPlane, float farPlane)
	{
		_projection = glm::perspective(glm::degrees(fov), aspect, nearPlane, farPlane);
	}
};

class target_camera : public camera
{
public:
	target_camera() : camera() { }
	~target_camera() { }
	void update(float deltaTime);
};

class first_person_camera : public camera
{
private:
	glm::quat _rotation;
	glm::vec3 _translation;
public:
	first_person_camera() { }
	~first_person_camera() { }
	glm::quat getRotation() { return _rotation; }
	void setRotation(const glm::quat& value) { _rotation = value; }
	void update(float deltaTime);
	void rotate(float deltaYaw, float deltaPitch);
	void move(const glm::vec3& translation);
};

class arc_ball_camera : public camera
{
private:
	float _rotX;
	float _rotY;
	float _minRotY;
	float _maxRotY;
	float _distance;
	float _minDistance;
	float _maxDistance;
public:
	arc_ball_camera() { }
	~arc_ball_camera() { }
	float getRotationX() const { return _rotX; }
	void setRotationX(float value) { _rotX = value; }
	float getRotationY() const { return _rotY; }
	void setRotationY(float value) { _rotY = value; }
	float getMinRotationY() const { return _minRotY; }
	void setMinRotationY(float value) { _minRotY = value; }
	float getMaxRotationY() const { return _maxRotY; }
	void setMaxRotationY(float value) { _maxRotY = value; }
	float getDistance() { return _distance; }
	void setDistance(float value) { _distance = value; }
	float getMinDistance() { return _minDistance; }
	void setMinDistance(float value) { _minDistance = value; }
	float getMaxDistance() { return _maxDistance; }
	void setMaxDistance(float value) { _maxDistance = value; }
	void update(float deltaTime);
	void move(float movement);
	void rotate(float deltaX, float deltaY);
	void translate(const glm::vec3& translation);
};