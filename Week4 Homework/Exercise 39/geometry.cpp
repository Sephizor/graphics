#include "geometry.h"


glm::vec3 tetrahedron_vertices[4] =
{
	glm::vec3(0.0f, 1.0f, 0.0f),
	glm::vec3(-1.0f, -1.0f, 1.0f),
	glm::vec3(1.0f,-1.0f, 1.0f),
	glm::vec3(0.0f,-1.0f, -1.0f)
};

glm::vec3 pyramid_vertices[5] =
{
	glm::vec3(0.0f, 1.0f, 0.0f),
	glm::vec3(-1.0f, -1.0f, 1.0f),
	glm::vec3(1.0f, -1.0f, 1.0f),
	glm::vec3(1.0f, -1.0f, -1.0f),
	glm::vec3(-1.0f, -1.0f, -1.0f),
};

glm::vec3 box_vertices[8] =
{
	glm::vec3(-1.0f, 1.0f,1.0f),
	glm::vec3(1.0f, 1.0f,1.0f),
	glm::vec3(1.0f,-1.0f,1.0f),
	glm::vec3(-1.0f,-1.0f,1.0f),
	glm::vec3(1.0f, 1.0f,-1.0f),
	glm::vec3(-1.0f,1.0f,-1.0f),
	glm::vec3(-1.0f,-1.0f,-1.0f),
	glm::vec3(1.0f,-1.0f,-1.0f)
};

glm::vec3 box_normals[8] =
{
	glm::normalize(glm::vec3(-1.0f, 1.0f, 1.0f)),
	glm::normalize(glm::vec3(1.0f, 1.0f, 1.0f)),
	glm::normalize(glm::vec3(1.0f, -1.0f, 1.0f)),
	glm::normalize(glm::vec3(-1.0f, -1.0f, 1.0f)),
	glm::normalize(glm::vec3(1.0f, 1.0f, -1.0f)),
	glm::normalize(glm::vec3(-1.0f, 1.0f, -1.0f)),
	glm::normalize(glm::vec3(-1.0f, -1.0f, -1.0f)),
	glm::normalize(glm::vec3(1.0f, -1.0f, -1.0f))
};



unsigned int tetrahedron_indices[12] = 
{
	//Side 1
	0, 2, 1,
	//Side 2
	0, 3, 2,
	//Side 3
	0, 1, 3,
	//Bottom
	1, 2, 3
};

unsigned int pyramid_indices[18] =
{
	0, 2, 1,
	0, 3, 2,
	0, 4, 3,
	0, 1, 4,
	1, 3, 4,
	1, 2, 3
};

unsigned int box_indices[36] = 
{
	//Side 1
	0, 1, 3,
	1, 2, 3,
	//Side 2
	1, 4, 2,
	4, 7, 2,
	//Side 3
	4, 5, 7,
	5, 6, 7,
	//Side 4
	5, 0, 6,
	0, 3, 6,
	//Side 5
	5, 4, 0,
	4, 1, 0,
	//Side 6	
	3, 2, 6,
	2, 7, 6
};

geometry* createBox()
{
	geometry* geom = new geometry();
	for(int i = 0;i < 8;++i)
	{
		geom->normals.push_back(box_normals[i]);
		geom->vertices.push_back(box_vertices[i]);
	}
	for(int i = 0;i < 36;++i)
		geom->indices.push_back(box_indices[i]);

	glGenVertexArrays(1, &geom->vao);
	glBindVertexArray(geom->vao);

	glGenBuffers(1, &geom->vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, geom->vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, geom->vertices.size() * sizeof(glm::vec3), &geom->vertices[0], GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	glGenBuffers(1, &geom->normalBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, geom->normalBuffer);
	glBufferData(GL_ARRAY_BUFFER, geom->normals.size() * sizeof(glm::vec3), &geom->normals[0], GL_STATIC_DRAW);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(1);

	glBindVertexArray(0);

	glGenBuffers(1, &geom->indexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, geom->indexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, geom->indices.size() * sizeof(unsigned int), &geom->indices[0], GL_STATIC_DRAW);
	return geom;
}

geometry* createTetrahedron()
{
	geometry* geom = new geometry();
	for(int i = 0;i < 4;++i)
		geom->vertices.push_back(tetrahedron_vertices[i]);
	for(int i = 0;i < 12;++i)
		geom->indices.push_back(tetrahedron_indices[i]);

	glGenVertexArrays(1, &geom->vao);
	glBindVertexArray(geom->vao);

	glGenBuffers(1, &geom->vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, geom->vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, geom->vertices.size() * sizeof(glm::vec3), &geom->vertices[0], GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	glBindVertexArray(0);

	glGenBuffers(1, &geom->indexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, geom->indexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, geom->indices.size() * sizeof(unsigned int), &geom->indices[0], GL_STATIC_DRAW);
	return geom;
}

geometry* createPyramid()
{
	geometry* geom = new geometry();
	for(int i = 0;i < 5;++i)
		geom->vertices.push_back(pyramid_vertices[i]);
	for(int i = 0;i < 18;++i)
		geom->indices.push_back(pyramid_indices[i]);

	glGenVertexArrays(1, &geom->vao);
	glBindVertexArray(geom->vao);

	glGenBuffers(1, &geom->vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, geom->vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, geom->vertices.size() * sizeof(glm::vec3), &geom->vertices[0], GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	glBindVertexArray(0);

	glGenBuffers(1, &geom->indexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, geom->indexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, geom->indices.size() * sizeof(unsigned int), &geom->indices[0], GL_STATIC_DRAW);
	return geom;
}

geometry* createDisks(int slices)
{
	geometry* geom = new geometry();
	glm::vec3 centre(0.0f, 0.0f, 0.0f);
	glm::vec3 vertex_prev(1.0f, 0.0f, 0.0f);
	glm::vec3 vertex_current;
	float deltaAngle = (2.0f * glm::pi<float>()) / slices;

	for(int i=1; i<=slices; ++i)
	{
		vertex_current = glm::vec3(cos(i * deltaAngle), 0.f, sin(i * deltaAngle));
		geom->vertices.push_back(centre);
		geom->vertices.push_back(vertex_prev);
		geom->vertices.push_back(vertex_current);
		vertex_prev = vertex_current;
	}

	glGenVertexArrays(1, &geom->vao);
	glBindVertexArray(geom->vao);

	glGenBuffers(1, &geom->vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, geom->vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, geom->vertices.size() * sizeof(glm::vec3), &geom->vertices[0], GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	glBindVertexArray(0);
	return geom;
}

geometry* createCylinder(int stacks, int slices)
{
	geometry* geom = new geometry();
	//Create top
	glm::vec3 centre(0.0f, 1.0f, 0.0f);
	glm::vec3 vertex_prev(1.0f, 1.0f, 0.0f);
	glm::vec3 vertex_current;
	float deltaAngle = (2 * glm::pi<float>()) / slices;

	for(int i=1; i<=slices; ++i)
	{
		vertex_current = glm::vec3(cos(i * deltaAngle), 1.0f, sin(i * deltaAngle));
		geom->vertices.push_back(centre);
		geom->vertices.push_back(vertex_prev);
		geom->vertices.push_back(vertex_current);
		vertex_prev = vertex_current;
	}

	//Create bottom
	centre = glm::vec3(0.0f, -1.0f, 0.0f);
	vertex_prev = glm::vec3(1.0f, -1.0f, 0.0f);
	for(int i=1; i<=slices; ++i)
	{
		vertex_current = glm::vec3(cos(i * deltaAngle), -1.0f, sin(i * deltaAngle));
		geom->vertices.push_back(centre);
		geom->vertices.push_back(vertex_prev);
		geom->vertices.push_back(vertex_current);
		vertex_prev = vertex_current;
	}

	//Create stacks
	glm::vec3 vertices[4];
	float deltaHeight = 2.0f / stacks;
	for(int i=0; i<stacks; ++i)
	{
		for(int j=0; j<slices; ++j)
		{
			vertices[0] = glm::vec3(cos(j * deltaAngle), 1.0f - (deltaHeight * i), sin(j * deltaAngle));
			vertices[1] = glm::vec3(cos((j+1) * deltaAngle), 1.0f - (deltaHeight * i), sin((j+1) * deltaAngle));
			vertices[2] = glm::vec3(cos(j * deltaAngle), 1.0f - (deltaHeight * (i+1)), sin(j * deltaAngle));
			vertices[3] = glm::vec3(cos((j+1) * deltaAngle), 1.0f - (deltaHeight * (i+1)), sin((j+1) * deltaAngle));

			//Triangle 1
			geom->vertices.push_back(vertices[0]);
			geom->vertices.push_back(vertices[3]);
			geom->vertices.push_back(vertices[2]);
			//Triangle 2
			geom->vertices.push_back(vertices[0]);
			geom->vertices.push_back(vertices[1]);
			geom->vertices.push_back(vertices[3]);
		}
	}

	glGenVertexArrays(1, &geom->vao);
	glBindVertexArray(geom->vao);

	glGenBuffers(1, &geom->vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, geom->vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, geom->vertices.size() * sizeof(glm::vec3), &geom->vertices[0], GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	glBindVertexArray(0);
	return geom;
}

geometry* createSphere(int stacks, int slices)
{
	geometry* geom = new geometry();
	//Create required values
	float deltaRho = glm::pi<float>() / stacks;
	float deltaTheta = 2.0f * glm::pi<float>() / slices;

	for(int i=0; i<stacks; ++i)
	{
		float rho = i * deltaRho;
		glm::vec3 vertices[4];
		for(int j=0; j<slices; ++j)
		{
			//Vertex 0
			float theta = j * deltaTheta;
			vertices[0] = glm::vec3(-sin(theta) * sin(rho), cos(theta) * sin(rho), cos(rho));
			//Vertex 1
			vertices[1] = glm::vec3(-sin(theta) * sin(rho + deltaRho), cos(theta) * sin(rho + deltaRho), cos(rho + deltaRho));

			theta = ((j+1) == slices) ? 0.0f : (j+1) * deltaTheta;
			//Vertex 2
			vertices[2] = glm::vec3(-sin(theta) * sin(rho), cos(theta) * sin(rho), cos(rho));
			//Vertex 3
			vertices[3] = glm::vec3(-sin(theta) * sin(rho + deltaRho), cos(theta) * sin(rho + deltaRho), cos(rho + deltaRho));

			//Triangle 1
			geom->vertices.push_back(vertices[0]);
			geom->vertices.push_back(vertices[1]);
			geom->vertices.push_back(vertices[2]);

			//Triangle 2
			geom->vertices.push_back(vertices[1]);
			geom->vertices.push_back(vertices[3]);
			geom->vertices.push_back(vertices[2]);
		}
	}

	glGenVertexArrays(1, &geom->vao);
	glBindVertexArray(geom->vao);

	glGenBuffers(1, &geom->vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, geom->vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, geom->vertices.size() * sizeof(glm::vec3), &geom->vertices[0], GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	glBindVertexArray(0);
	return geom;
}

geometry* createTorus(float radius, int stacks, int slices)
{
	geometry* geom = new geometry();
	float deltaStack = 2.0f * glm::pi<float>() / stacks;
	float deltaSlice = 2.0f * glm::pi<float>() / slices;
	for(int i=0; i<stacks; ++i)
	{
		float a0 = i * deltaStack;
		float a1 = a0 + deltaStack;
		glm::vec3 vertices[4];
		for(int j=0; j<slices; ++j)
		{
			float c = cos(j * deltaSlice);
			float r = c + radius;
			vertices[0] = glm::vec3(sin(a0) * r, sin(j * deltaSlice), cos(a0) * r);
			vertices[1] = glm::vec3(sin(a1) * r, sin(j * deltaSlice), cos(a1) * r);
			c = cos((j + 1) * deltaSlice);
			r = c + radius;
			vertices[2] = glm::vec3(sin(a0) * r, sin((j + 1) * deltaSlice), cos(a0) * r);
			vertices[3] = glm::vec3(sin(a1) * r, sin((j + 1) * deltaSlice), cos(a1) * r);

			//Triangle 1
			geom->vertices.push_back(vertices[0]);
			geom->vertices.push_back(vertices[1]);
			geom->vertices.push_back(vertices[2]);

			//Triangle 2
			geom->vertices.push_back(vertices[1]);
			geom->vertices.push_back(vertices[3]);
			geom->vertices.push_back(vertices[2]);
		}
	}

	glGenVertexArrays(1, &geom->vao);
	glBindVertexArray(geom->vao);

	glGenBuffers(1, &geom->vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, geom->vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, geom->vertices.size() * sizeof(glm::vec3), &geom->vertices[0], GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(0);

	glBindVertexArray(0);
	return geom;
}